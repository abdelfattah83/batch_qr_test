#ifdef SLDA
#undef SLDA
#endif


#define SLDA(n)    ( ((n+1)%4) == 0 ? (n) : (n+1) )
#define sA(i,j)    sA[(j) * slda + (i)]

template<typename T, int N>
__global__ void
geqrf_batched_smallsq_1_kernel(
    T **dA_array, int ldda,
    T **dtau_array, int *info_array,
    int batchCount)
{
    extern __shared__ float shared_data[];
    T* sdata1 = (T*)shared_data;

    const int tx = threadIdx.x;
    const int ty = threadIdx.y;
    const int batchid = blockIdx.x * blockDim.y + ty;
    if(batchid >= batchCount) return;
    if(tx >= N) return;

    const int slda  = SLDA(N);
    T* dA           = dA_array[batchid];
    T* dtau         = dtau_array[batchid];
    int* info       = &info_array[batchid];

    // shared memory pointers
    T* sA  = (T*)(sdata1 + ty * slda * N);
    T* sdw = (T*)(sdata1 + blockDim.y * slda * N);
    sdw += ty * N;

    T rA[N] = {0};
    T alpha, tau, tmp, zsum, scale = 0;
    T sum = 0, norm = 0, beta;

    if( tx == 0 ){
        (*info) = 0;
    }

    // init tau
    dtau[tx] = 0;
    // read
    #pragma unroll
    for(int i = 0; i < N; i++){
        rA[i] = dA[ i * ldda + tx ];
    }

    #pragma unroll
    for(int i = 0; i < N-1; i++){
        sA[ i * slda + tx] = rA[i];
        sdw[tx] = rA[i] * rA[i];
        __syncthreads();
        alpha = sA[i * slda + i];
        sum = 0;
        #pragma unroll
        for(int j = i; j < N; j++){
            sum += sdw[j];
        }
        norm = sqrt(sum);
        beta = -copysign(norm, alpha);
        scale = 1 / (alpha-beta);
        tau = (beta - alpha) / beta;

        if(tx == i){
            dtau[i] = tau;
        }

        tmp = (tx == i) ? beta : rA[i] * scale;

        if(tx >= i){
            rA[i] = tmp;
        }

        dA[ i * ldda + tx ] = rA[i];
        rA[i] = (tx == i) ? 1  : rA[i];
        rA[i] = (tx < i ) ? 0 : rA[i];
        tmp = rA[i] * tau;

        __syncthreads();
        #pragma unroll
        for(int j = i+1; j < N; j++){
            sA[j * slda + tx] = rA[j] * tmp;
        }
        __syncthreads();

        zsum = 0;
        #pragma unroll
        for(int j = i; j < N; j++){
            zsum += sA[tx * slda + j];
        }
        sA[tx * slda + N] = zsum;
        __syncthreads();

        #pragma unroll
        for(int j = i+1; j < N; j++){
            rA[j] -= rA[i] * sA[j * slda + N];
        }
        __syncthreads();
    }
    // write the last column
    dA[ (N-1) * ldda + tx ] = rA[N-1];
}

template<typename T>
int
geqrf_batched_smallsq_1(
    int n,
    T** dA_array, int ldda,
    T **dtau_array, int* info_array,
    int batchCount, cudaStream_t stream )
{
    int arginfo = 0;
    if( (n < 0) || ( n > 32 ) ){
        return -1;
    }

    if( n == 0) return 0;

    const int ntcol = 1; //max(1, 32/m);

    int shmem  = ( SLDA(n) * n * sizeof(T) );
    shmem     += ( n * sizeof(T) );
    shmem     *= ntcol;
    int nth = n;
    int gridx = (batchCount+ntcol-1) / ntcol;
    dim3 grid(gridx, 1, 1);
    dim3 threads(nth, ntcol, 1);

    switch(n){

        case  1: geqrf_batched_smallsq_1_kernel<T, 1><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case  2: geqrf_batched_smallsq_1_kernel<T, 2><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case  3: geqrf_batched_smallsq_1_kernel<T, 3><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case  4: geqrf_batched_smallsq_1_kernel<T, 4><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case  5: geqrf_batched_smallsq_1_kernel<T, 5><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case  6: geqrf_batched_smallsq_1_kernel<T, 6><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case  7: geqrf_batched_smallsq_1_kernel<T, 7><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case  8: geqrf_batched_smallsq_1_kernel<T, 8><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case  9: geqrf_batched_smallsq_1_kernel<T, 9><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 10: geqrf_batched_smallsq_1_kernel<T,10><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 11: geqrf_batched_smallsq_1_kernel<T,11><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 12: geqrf_batched_smallsq_1_kernel<T,12><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 13: geqrf_batched_smallsq_1_kernel<T,13><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 14: geqrf_batched_smallsq_1_kernel<T,14><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 15: geqrf_batched_smallsq_1_kernel<T,15><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 16: geqrf_batched_smallsq_1_kernel<T,16><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 17: geqrf_batched_smallsq_1_kernel<T,17><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 18: geqrf_batched_smallsq_1_kernel<T,18><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 19: geqrf_batched_smallsq_1_kernel<T,19><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 20: geqrf_batched_smallsq_1_kernel<T,20><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 21: geqrf_batched_smallsq_1_kernel<T,21><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 22: geqrf_batched_smallsq_1_kernel<T,22><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 23: geqrf_batched_smallsq_1_kernel<T,23><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 24: geqrf_batched_smallsq_1_kernel<T,24><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 25: geqrf_batched_smallsq_1_kernel<T,25><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 26: geqrf_batched_smallsq_1_kernel<T,26><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 27: geqrf_batched_smallsq_1_kernel<T,27><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 28: geqrf_batched_smallsq_1_kernel<T,28><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 29: geqrf_batched_smallsq_1_kernel<T,29><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 30: geqrf_batched_smallsq_1_kernel<T,30><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 31: geqrf_batched_smallsq_1_kernel<T,31><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        case 32: geqrf_batched_smallsq_1_kernel<T,32><<<grid, threads, shmem, stream>>>(dA_array, ldda, dtau_array, info_array, batchCount); break;
        default: printf("error: size %lld is not supported\n", (long long) n);
    }
    return arginfo;
}

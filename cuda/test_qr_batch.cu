#include<stdio.h>
#include<stdlib.h>
#include <sys/time.h>
#include<vector>
#include<omp.h>
#include<cuda_runtime.h>
#include"geqrf_batched_smallsq.h"
#include"geqrf_batched_smallsq_shmem.h"

#define FMULS_GEQRF(m_, n_) (((m_) > (n_)) \
    ? ((n_) * ((n_) * (  0.5-(1./3.) * (n_) + (m_)) +    (m_) + 23. / 6.)) \
    : ((m_) * ((m_) * ( -0.5-(1./3.) * (m_) + (n_)) + 2.*(n_) + 23. / 6.)) )

#define FADDS_GEQRF(m_, n_) (((m_) > (n_)) \
    ? ((n_) * ((n_) * (  0.5-(1./3.) * (n_) + (m_))           +  5. / 6.)) \
    : ((m_) * ((m_) * ( -0.5-(1./3.) * (m_) + (n_)) +    (n_) +  5. / 6.)) )

#define FLOPS_GEQRF(m_, n_) (FMULS_GEQRF((double)(m_), (double)(n_)) + FADDS_GEQRF((double)(m_), (double)(n_)) )


////////////////////////////////////////////////////////////////////////////////
template<typename T>
void print_matrix( int m, int n,  T* A, int lda)
{
    printf("\n[\n");
    for(int i = 0; i < m; i++) {
        for(int j = 0; j < n; j++) {
            printf("%8.4f", A[j*lda + i]);
            printf("%c  ", (j < n-1)? ',' : ';');
        }
        printf("%s", (i < m-1) ? "\n" : "]\n");
    }
    printf("\n");
}

////////////////////////////////////////////////////////////////////////////////
double wall_time( void )
{
    struct timeval t;
    gettimeofday( &t, NULL );
    return t.tv_sec + t.tv_usec*1e-6;
}

////////////////////////////////////////////////////////////////////////////////
template<typename T>
void simple_cpu_gemm(int n, T* hA, T* hB, T* hC)
{
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++) {
            T r = 0;
            for(int k = 0; k < n; k++) {
                r += hA[k * n + i] * hB[ j * n + k];
            }
            hC[j * n + i] = r;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
template<typename T>
void generate_q(int n, T* hQR, T* hTAU, T* hQ, T* hV, T* hW)
{
    // init hQ to identity
    memset(hQ, 0, n * n * sizeof(T));
    for(int i = 0; i < n; i++) {
        hQ[i * n + i] = 1.;
    }

    for(int i = n-1; i >= 0; i--) {
        T tau = hTAU[i];

        // get the full reflector in hV
        memset(hV, 0, n * sizeof(T));
        hV[i] = 1.;
        memcpy( &hV[i+1], &hQR[(i*n) + (i+1)], (n-1-i) * sizeof(T) );

        // multiply hQ by tau*hV' ==> hW
        for(int j = 0; j < n; j++) {
            hW[j] = 0.;
            for(int k = 0; k < n; k++) {
                hW[j] += hQ[(j*n) + k] * hV[k];
            }
            hW[j] *= tau;
        }

        // hQ - hV * hW
        for(int j = 0; j < n; j++) {
            for(int k = 0; k < n; k++) {
                hQ[(j*n) + k] -= hV[k] * hW[j];
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
template<typename T>
double check_qr(int n, T* hA, T* hQR, T* hTAU)
{
    // workspaces
    std::vector<T> hA2(n*n, 0.);
    std::vector<T> hR(n*n);
    std::vector<T> hQ(n*n);
    std::vector<T> hV(n);
    std::vector<T> hW(n);

    // generate Q
    generate_q( n, hQR, hTAU, hQ.data(), hV.data(), hW.data());

    // extract R
    for(int j = 0; j < n; j++) {
        for(int i = 0; i <= j; i++) {
            hR[(j*n)+i] = hQR[(j*n)+i];
        }
    }

    // gemm
    simple_cpu_gemm<T>(n, hQ.data(), hR.data(), hA2.data());

    // compute error
    double normA = 0;
    double normD = 0;
    for(int i = 0; i < n*n; i++) {
        normA += hA[i] * hA[i];
        normD += (hA[i] - hA2[i]) * (hA[i] - hA2[i]);
    }
    normA = sqrt(normA);
    normD = sqrt(normD);
    return normD / (n * normA);
}

////////////////////////////////////////////////////////////////////////////////
template<typename T>
void qr_test(int kernel_id, int n, int batch, double &elapsed_time, double &performance, double &maxerror)
{
    if(kernel_id < 1 || kernel_id > 2) {
        kernel_id = 1; // default to 1st kernel
    }

    size_t sizeA   = batch * n * n;
    size_t sizeTAU = batch * n * 1;
    const double flops = ( batch * FLOPS_GEQRF(n, n) ) / 1e9;

    // alloc host
    std::vector<T> hA(   sizeA );
    std::vector<T> hQR(  sizeA );
    std::vector<T> hTAU( sizeTAU, 0.);

    // info
    std::vector<int> hinfo( batch, 0.);

    // init cpu data
    unsigned int seed;
    #pragma omp parallel private(seed)
    {
        seed = omp_get_thread_num();
        #pragma omp for
        for(int i = 0; i < sizeA; ++i) {
            hA[i] = rand_r(&seed) / (T)RAND_MAX;
        }
    }

    // alloc device
    T *dA=NULL, *dTAU=NULL;
    int *dinfo=NULL;
    cudaMalloc( (void**)&dA,    sizeA   * sizeof(T) );
    cudaMalloc( (void**)&dTAU,  sizeTAU * sizeof(T) );
    cudaMalloc( (void**)&dinfo, batch   * sizeof(int) );

    // send data
    cudaMemcpy( dA,    hA.data(),    sizeA * sizeof(T),   cudaMemcpyHostToDevice );
    cudaMemcpy( dinfo, hinfo.data(), batch * sizeof(int), cudaMemcpyHostToDevice );

    // setup pointers
    T **dA_array=NULL, **dTAU_array=NULL;
    std::vector<T*> hA_array(   batch );
    std::vector<T*> hTAU_array( batch );
    cudaMalloc( (void**)&dA_array,   batch * sizeof(T*) );
    cudaMalloc( (void**)&dTAU_array, batch * sizeof(T*) );

    for(int i = 0; i < batch; i++) {
        hA_array[i]   = dA   + i * n * n;
        hTAU_array[i] = dTAU + i * n;
    }
    cudaMemcpy(dA_array,   hA_array.data(),   batch * sizeof(T*), cudaMemcpyHostToDevice);
    cudaMemcpy(dTAU_array, hTAU_array.data(), batch * sizeof(T*), cudaMemcpyHostToDevice);

    // create stream/event
    cudaStream_t stream;
    cudaStreamCreate( &stream );

    // launch
    elapsed_time = wall_time();

    if(kernel_id == 1) {
        geqrf_batched_smallsq_1<T>(n, dA_array, n, dTAU_array, dinfo, batch, stream );
    }
    else if (kernel_id == 2) {
        geqrf_batched_smallsq_2<T>(n, dA_array, n, dTAU_array, dinfo, batch, stream );
    }

    cudaStreamSynchronize( stream );

    elapsed_time = wall_time() - elapsed_time;
    performance = flops / elapsed_time;

    // get data
    cudaMemcpy( hQR.data(),  dA,   sizeA   * sizeof(T), cudaMemcpyDeviceToHost );
    cudaMemcpy( hTAU.data(), dTAU, sizeTAU * sizeof(T), cudaMemcpyDeviceToHost );

    // compute error
    double error = 0;
    #pragma omp parallel for reduction(max:error)
    for(int i = 0; i < batch; i++) {
        double err = check_qr<T>(n, hA.data() + (i*n*n), hQR.data() + (i*n*n), hTAU.data() + (i*n) );
        error = max(error, err);
    }
    maxerror = error;

    // free data
    cudaFree( dA );
    cudaFree( dTAU );
    cudaFree( dinfo );
    cudaFree( dA_array );
    cudaFree( dTAU_array );

    cudaStreamDestroy( stream );
}


int main( int argc, char* argv[] )
{
    char p            = 's';
    int  n            = 16;
    int  batch        = 1000;
    int  print_header = 0;
    int  niter        = 1;

    if( argc > 1 ) {
        p = *argv[1];
    }

    if( argc > 2 ) {
        n = atoi( argv[2] );
    }

    if( argc > 3 ) {
        batch = atoi( argv[3] );
    }

    if( argc > 4 ) {
        niter = atoi( argv[4] );
    }

    if( argc > 5 ) {
        print_header = atoi( argv[5] );
    }

    double elapsed_time=0, performance=0, maxerror = 0;

    elapsed_time=0; performance=0; maxerror = 0;

    if(print_header != 0) {
        printf("# Kernel 1: Nx1 threads per QR, A is in register, shared memory is aux.\n");
        printf("# Kernel 2: Nx1 threads per QR, A is in shared memory\n");
        printf("#                                      Kernel 1                           Kernel 2            \n");
        printf("# Precision      N  Batch  [ time (ms),   Gflop/s,   error ] [ time (ms),   Gflop/s,   error ]\n");
        printf("# --------------------------------------------------------------------------------------------\n");
    }

    for(int iter = 0; iter < niter; ++iter) {
        if( p == 's' ) {
            printf("  %9s  %5d  %5d  ", "Single", n, batch);

            // kernel 1
            qr_test<float>(1, n, batch, elapsed_time, performance, maxerror);
            printf("  %9.2f  %9.2f  %6.1e   ", elapsed_time*1000., performance, maxerror);

            // kernel 2
            qr_test<float>(2, n, batch, elapsed_time, performance, maxerror);
            printf("  %9.2f  %9.2f  %6.1e\n", elapsed_time*1000., performance, maxerror);

        }
        else if( p == 'd' ) {
            printf("  %9s  %5d  %5d  ", "Double", n, batch);

            // kernel 1
            qr_test<double>(1, n, batch, elapsed_time, performance, maxerror);
            printf("  %9.2f  %9.2f  %6.1e   ", elapsed_time*1000., performance, maxerror);

            // kernel 2
            qr_test<double>(2, n, batch, elapsed_time, performance, maxerror);
            printf("  %9.2f  %9.2f  %6.1e\n", elapsed_time*1000., performance, maxerror);
        }
        else {
            continue;
        }
    }
}
